###UGUI 屏幕自适应

**Canvas Scaler**

UGUI的屏幕自适应主要是用到了Canvas的Canvas Scaler组件，设置属性Ui Scale Mode为Scale With Screen Size，设置Reference Resolution(参考分辨率)X、Y为你期望的大小，设置Screen Match Mode为Expand。

**Canvas**

为了能在UI层上面做些花样(比如加特效)，我们需要再对Canvas的Canvas组件进行设置。设置Render Mode为Screen Space - Camera，这样我们就需要给它指定一台Reader Camera，我们待会再设置。然后设置Plane Distance属性为0。

![Alt text](../images/1421290797801.png)

现在我们来给Canvas创建一个Reader Camera，我们在Canvas下面创建两台摄像机，分别命名为UICamera和UIForward，UICamera就是我们的ReaderCamera，而UIForward是用来渲染UI层之上的物件的。

![Alt text](../images/1421291971994.png)


**UICamera**

设置如下图所示，需要注意的是Size属性应该为Canvas的Canvas Scaler组件的Reference Resolution属性的X/Y。我们这里的Size为960/640=1.5。

![Alt text](../images/1421291563866.png)

**UIForward**
Copy UICamera的属性到UIForward，更改Culling Mask选择UIForward Layer，没有的话就新建一个层。更改Depth属性为1，或其他值，保证三个摄像机的Depth属性值为 UIForward > UICamera > Main Camera。

把需要放在UI层前面显示的物件的Layer设置为UIForward。

**Main Camera**

UI的自适应UGUI帮我们做好了，接下来要给3DCamera加上自适应的功能。在Main Camera上加上以下代码。需要手动更改代码或者在编辑器里设置ManualWidth和ManualHeight的值。

*CameraScale.cs*

``` c#
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CameraScale : MonoBehaviour
{
    public int ManualWidth = 960;
    public int ManualHeight = 640;

    void Start()
    {
        int manualHeight;
        if (System.Convert.ToSingle(Screen.height) / Screen.width > System.Convert.ToSingle(ManualHeight) / ManualWidth)
            manualHeight = Mathf.RoundToInt(System.Convert.ToSingle(ManualWidth) / Screen.width * Screen.height);
        else
            manualHeight = ManualHeight;
        Camera camera = GetComponent<Camera>();
        float scale = System.Convert.ToSingle(manualHeight / ManualHeight);
        camera.fieldOfView *= scale;
    }
}
```