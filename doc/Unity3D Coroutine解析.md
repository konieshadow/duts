###Unity3D Coroutine解析

Unity中的 Coroutine 并不是真正意义上的协程，更不是多线程，它只是Unity利用c#的IEnumerator与yield return语法所封装的一个方便开发者使用的工具。用它实现的大部分功能都是可以用其他方式来实现的。

----
#####IEnumerator与yield return

IEnumerator是一个c#接口，它有一个公开属性和两个公开方法：

> Current属性，获取集合中的当前元素；
MoveNext方法，将枚举数推进到集合的下一个元素；
Reset方法，将枚举数设置为其初始位置，该位置位于集合中第一个元素之前。

实现了IEnumerator的对象便可使用foreach来进行循环迭代。但是要实现上述的方法和属性也显得比价麻烦。为此，c#提供了yield return关键字来快捷的创建IEnumerator接口对象。

只要一个方法里出现了yield return，那么这个方法的返回值便是IEnumerator。

``` c#

public class PowersOf2
{
    static void Main()
    {
        // Display powers of 2 up to the exponent of 8:
        foreach (int i in Power(2, 8))
        {
            Console.Write("{0} ", i);
        }
    }

    public static System.Collections.IEnumerable<int> Power(int number, int exponent)
    {
        int result = 1;

        for (int i = 0; i < exponent; i++)
        {
            result = result * number;
            yield return result;
        }
    }

    // Output: 2 4 8 16 32 64 128 256
}
```

Unity中的yield return便来自这里。

----
#####StartCoroutine方法
StartCoroutine方法接受一个IEnumerator类型的对象或者一个字符串做参数，如果是字符串的话，它会寻找当前类中的这个方法，方法的返回值必须是IEnumerator类型，因为它会把返回值传递给另一个重载的方法。

当StartCoroutine方法执行后，就会对IEnumerator对象进行迭代，每帧一次，直到遍历完毕，也就是说yield return所在方法的所有代码都执行完了。

我们经常配合StartCoroutine使用Unity自带的WaitForSeconds方法，下面我们自己来实现一个WaitForSeconds。

``` c#
using UnityEngine;
using System.Collections;

public class TimerTest : MonoBehaviour
{
    IEnumerator Start()
    {
        yield return StartCoroutine(MyWaitFunction(1.0f));
        print("1");
        yield return StartCoroutine(MyWaitFunction(2.0f));
        print("2");
    }

    IEnumerator MyWaitFunction(float delay)
    {
        float timer = Time.time + delay;
        while (Time.time < timer)
        {
            yield return null;
        }
    }
}
```

虽然Coroutine并没有太多的神奇之处，但使用Coroutine在某些时候确实能简化代码。