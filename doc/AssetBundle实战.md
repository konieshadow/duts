###AssetBundle实战

----
####AssetBundle原理
AssetBundle是Unity Pro提供的功能，用于将游戏资源打包成二进制文件并通过网络或本地读取解包。

AssetBundle可以打包的资源可以分为六大类：界面、模型、特效、声音、场景、脚本。

目前的最佳实践是这样的：

> 1. 游戏发布或升级时，将所有游戏资源导出成AssetBundle(必须禁用压缩选项，因为后面要用AssetBundle.CreateFromFile()来读取)。
> 2. 使用GZIP格式(或其他压缩格式)压缩后上传至服务器。
> 3. 当软件(游戏)开始运行时比对本地版本号与服务器返回的最新版本号，如果不同则下载最新版的AssetBundle压缩包。
> 4. 解压AssetBundle至应用程序的数据目录，修改本地版本号为最新版本号。
> 5. 进入场景时(或者从AssetBundle载入场景时)使用AssetBundle.CreateFromFile()来动态加载相应的游戏资源。

----
#### 资源打包
打包时需要对不同的平台分别打包，BuildPipeline.BuildAssetBundle()方法的第四个参数targetPlatform用于指定平台。

移动平台上的脚本更新会遇到问题，只有当本地有同名脚本时从AssetBundle读取的Prefab上绑定的脚本才会执行。所以脚本要留在本地，至少留个壳子。

Unity提供创建AssetBundle间依赖关系的接口，即BuildPipeline.PushAssetDependencies()和BuildPipeline.PopAssetDependencies()，但加载它们时需要严格根据依赖关系按顺序下载资源，这样在打包和加载时都会有许多麻烦，如果有其他解决方案能减少因为共有依赖关系造成的资源浪费我们便不考虑这种方法。

目前来看可以这样，将拥有相同依赖的不用资源各自做成Prefab，然后一起打包成一个AssetBundle。

所以我们的解决方案应该是这样的：

> * 模型部分，包括这个模型文件与动画文件，按角色用Prefab将所有模型打包成一个AssetBundle。
> * 特效部分，用Prefab全部打包成一个AssetBundle。
> * 声音部分，用Prefab全部打包成一个AssetBundle。
> * 场景部分，缺乏实践，暂不考虑。
> * 脚本部分，绑定在Prefab上。

打包的脚本：

MakeAssetBundle.cs
``` c#
using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeAssetBundle: Editor
{
  
  [MenuItem("AssetBundle/Create AssetBunldes Main")]
  static void CreateAssetBunldesMain ()
  {
        //获取在Project视图中选择的所有游戏对象
    Object[] SelectedAsset = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
 
        //遍历所有的游戏对象
    foreach (Object obj in SelectedAsset) 
    {
      //本地测试：建议最后将Assetbundle放在StreamingAssets文件夹下，如果没有就创建一个，因为移动平台下只能读取这个路径
      //StreamingAssets是只读路径，不能写入
      //服务器下载：就不需要放在这里，服务器上客户端用www类进行下载。
      string targetPath = Application.dataPath + "/StreamingAssets/" + obj.name + ".assetbundle";
      if (BuildPipeline.BuildAssetBundle (obj, null, targetPath, BuildAssetBundleOptions.CollectDependencies)) {
          Debug.Log(obj.name +"资源打包成功");
      } 
      else 
      {
        Debug.Log(obj.name +"资源打包失败");
      }
    }
    //刷新编辑器
    AssetDatabase.Refresh (); 
    
  }
  
  [MenuItem("AssetBundle/Create AssetBunldes ALL")]
  static void CreateAssetBunldesALL ()
  {
    
    Caching.CleanCache ();
 

    string Path = Application.dataPath + "/StreamingAssets/ALL.assetbundle";
 
    
    Object[] SelectedAsset = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
 
    foreach (Object obj in SelectedAsset) 
    {
      Debug.Log ("Create AssetBunldes name :" + obj);
    }
    
    //这里注意第二个参数就行
    if (BuildPipeline.BuildAssetBundle (null, SelectedAsset, Path, BuildAssetBundleOptions.CollectDependencies)) {
      AssetDatabase.Refresh ();
    } else {
      
    }
  }
  
  [MenuItem("AssetBundle/Create Scene")]
  static void CreateSceneALL ()
  {
    //清空一下缓存
    Caching.CleanCache();
    string Path = Application.dataPath + "/MyScene.unity3d";
    string  []levels = {"Assets/Level.unity"};
      //打包场景
      BuildPipeline.BuildPlayer( levels, Path,BuildTarget.WebPlayer, BuildOptions.BuildAdditionalStreamedScenes);
    AssetDatabase.Refresh ();
  }
  
}
```

----
####AssetBundle的读取与加载
#####读取
前面已经说了，我们使用AssetBundle.CreateFromFile()从文件读取AssetBundle，它的参数只有一个string类型的path，返回一个AssetBundle对象。

#####从AssetBundle中加载Assets
* AssteBundle.Load()
通过名字来加载Assets，同步。
* AssetBundle.LoadAsync()
通过名字来加载Assets，异步。
* AssetBundle.LoadAll()
加载所有的Assets，返回一个object数组，也可以传入一个类型来加载指定类型的资源，同步。